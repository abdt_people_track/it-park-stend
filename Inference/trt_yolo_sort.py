import cv2
import numpy as np
import sort
from time import sleep, time
from datetime import datetime
import logging
from pytz import timezone
import argparse
from utils.yolo_with_plugins import TrtYOLO
import pycuda.autoinit  # This is needed for initializing CUDA driver
from utils.yolo_classes import get_cls_dict
import subprocess


class BusCamera(object):
    def __init__(self, name_camera, path_to_input_video, path_to_output_video, logger, up=340, down=280, max_age=5, thresh=0.5, fourcc="XVID"):
        self.width = 416
        self.height = 416
        self.down = down
        self.up = up

        self.trt_yolo = TrtYOLO("yolov4-tiny-416x416", (self.height, self.width), 2)
        self.name_camera = name_camera
        self.fourcc = cv2.VideoWriter_fourcc(*fourcc)
        self.thresh = thresh  # confidence threshold

        self.path_to_input_video = path_to_input_video
        self.path_to_output_video = path_to_output_video

        self.tracker = sort.Sort(max_age)
        self.outcome = 0
        self.income = 0
        self.dic = {}
        self.logger = logger

    def __call__(self):
        self.video_writer()

    def open_camera(self, camera_url, latency=200, width=640, height=480, use_gstreamer=False):
        """Open an RTSP URI (IP CAM)."""
        print("Открываем видео поток ...")
        if use_gstreamer:
            gst_elements = str(subprocess.check_output("gst-inspect-1.0"))
            if "omxh264dec" in gst_elements:
                # Use hardware H.264 decoder on Jetson platforms
                gst_str = (
                    "rtspsrc location={} latency={} ! " "rtph264depay ! h264parse ! omxh264dec ! " "nvvidconv ! " "video/x-raw, width=(int){}, height=(int){}, " "format=(string)BGRx ! videoconvert ! " "appsink"
                ).format(camera_url, latency, width, height)
            elif "avdec_h264" in gst_elements:
                # Otherwise try to use the software decoder 'avdec_h264'
                # note: in case resizing images is necessary, try adding
                #       a 'videoscale' into the pipeline
                gst_str = ("rtspsrc location={} latency={} ! " "rtph264depay ! h264parse ! avdec_h264 ! " "videoconvert ! appsink").format(camera_url, latency)
            else:
                raise RuntimeError("H.264 decoder not found!")
            return cv2.VideoCapture(gst_str, cv2.CAP_GSTREAMER)
        else:
            return cv2.VideoCapture(camera_url)

    def rescale_consts(self, w, h):
        """Resize width, height, line up and down
        Args:
            w ([float]): Width of origin image.
            h ([float]): Height of origin image.
        """
        print(f"w: {w}, h: {h} of origin frame")
        self.out_width = int(w)
        self.out_height = int(h)
        self.width_scale = self.out_width / self.width
        self.height_scale = self.out_height / self.height
        self.down = int(down * self.height_scale)
        self.up = int(up * self.height_scale)
        self.line_centr = int((self.up - self.down) / 2) + self.down

    def video_writer(self):
        fps = 0.0
        cap = self.open_camera(self.path_to_input_video, latency=200, width=640, height=480, use_gstreamer=False)
        self.rescale_consts(cap.get(3), cap.get(4))
        out = cv2.VideoWriter(self.path_to_output_video, self.fourcc, 25.0, (self.out_width, self.out_height))

        tic = time()
        while cap.isOpened():
            ret, frame = cap.read()
            if not ret:
                print("Some error... ret is False ... Camera out of reach ... go avoid ...")
                self.logger.warning("unknown")
                break

            detects = self.prepare_to_tracking(frame)
            trackers = self.tracker.update(detects)
            self.track_and_draw_and_write(trackers, frame, out=out)
            toc = time()
            curr_fps = 1.0 / (toc - tic)
            fps = curr_fps if fps == 0.0 else (fps * 0.95 + curr_fps * 0.05)
            tic = toc
            print(fps)
            if cv2.waitKey(1) & 0xFF == ord("q"):
                break
        out.release()
        cap.release()
        cv2.destroyAllWindows()

    def image_detection(self, image):
        """Head function for detection via TensorRT.
        Args:
            image ([np.array]): Размерность исходная, т.е. shape как у входного видео.
        """
        boxes, confs, clss = self.trt_yolo.detect(image, self.thresh)
        return boxes, confs, clss

    def prepare_to_tracking(self, frame):
        boxes, confs, clss = self.image_detection(frame)
        detect_counts = len(boxes)
        if detect_counts != 0:
            detects = np.zeros((detect_counts, 5))
            for i, (box, conf, cl) in enumerate(zip(boxes, confs, clss)):
                confidence = float(conf / 100)  # [0;1]
                x1, y1, x2, y2 = box[0], box[1], box[2], box[3]
                detects[i, :] = np.array([x1, y1, x2, y2, confidence])
        else:
            detects = np.empty((0, 5))
        return detects

    def calc_center_bbox(self, point1, point2):
        x1, y1 = point1
        x2, y2 = point2
        return (int(x1 + (x2 - x1) / 2), int(y1 + (y2 - y1) / 2))

    def visualization(self, frame, point1, point2, id_track, d_0, d_1):
        result_img = cv2.rectangle(frame, point1, point2, (255, 0, 0), thickness=2)
        # result_img = cv2.circle(result_img, center_point, 2, (255, 0, 0), thickness=5)
        # result_img = cv2.line(result_img, (0, self.up), (self.out_width, self.up), (255, 0, 0))
        # result_img = cv2.line(result_img, (0, self.down), (self.out_width, self.down), (255, 0, 0))
        result_img = cv2.putText(result_img, "{}".format(id_track), (int(d_0), int(d_1) - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)
        # result_img = cv2.rectangle(result_img, (self.out_width - 120, 5), (self.out_width, 65), (255, 255, 255), -1)
        result_img = cv2.putText(result_img, f"income: {self.income}", (self.out_width - 100, 25), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)
        result_img = cv2.putText(result_img, f"Outcome: {self.outcome}", (self.out_width - 100, 50), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)
        return result_img

    def track_and_draw_and_write(self, trackers, frame, out=None):
        # print(len(trackers))
        for d in trackers:
            point1 = (int(d[0]), int(d[1]))
            point2 = (int(d[2]), int(d[3]))
            id_track = int(d[4])
            center_point = self.calc_center_bbox(point1, point2)
            if id_track not in self.dic:
                self.dic[id_track] = None
            self.counter(id_track, center_point[1])
            frame = self.visualization(frame, point1, point2, id_track, d[0], d[1])
        if len(trackers) != 0:
            pass
            # print(result_img.shape, "res_img")
            # out.write(result_img)
            # cv2.imshow("bus", result_img)
        else:
            pass
            # print(frame.shape, "frame_sh")
            # frame = cv2.rectangle(frame, (self.out_width - 120, 5), (self.out_width, 65), (255, 255, 255), -1)
            # frame = cv2.putText(frame, f"income: {self.income}", (self.out_width - 100, 25), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)
            # frame = cv2.putText(frame, f"Outcome: {self.outcome}", (self.out_width - 100, 50), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)
            # out.write(frame)
            # cv2.imshow("bus", frame)
        return None

    def counter(self, id_track, y):
        if self.dic[id_track] == "Untracked":
            return None
        if y >= self.down and y <= self.up:
            if self.dic[id_track] is None:
                if y < self.line_centr:
                    self.dic[id_track] = "Down"
                else:
                    self.dic[id_track] = "Up"
        else:
            if self.dic[id_track] == "Up" and y < self.down:
                self.income += 1
                #  логируем вход. указываем время, имя камеры, income
                self.logger.info("income")
                # self.log_write('income')
                self.dic[id_track] = "Untracked"
            if self.dic[id_track] == "Down" and y > self.up:
                self.outcome += 1
                #  логируем выход. указываем время, имя камеры, outcome
                self.logger.info("outcome")
                # self.log_write('outcome')
                self.dic[id_track] = "Untracked"
        return None


def init_logger(camera_name, output_file_name="result_video/camera_log.txt"):
    logger = logging.getLogger(camera_name)
    logger.setLevel(logging.INFO)
    file_handler = logging.FileHandler(output_file_name)
    logger.addHandler(file_handler)
    logging.Formatter.converter = lambda *args: datetime.now(tz=timezone("Europe/Moscow")).timetuple()
    formatter = logging.Formatter(fmt="%(asctime)s,%(name)s,%(message)s", datefmt="%Y-%m-%d %H:%M:%S")
    file_handler.setFormatter(formatter)
    logger.propagate = False
    return logger


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--camera", required=True, type=int)
    args = vars(parser.parse_args())
    rtsp = {
        1: "rtsp://178.207.8.252:6604/MTUsMywyMzcwMjgsMCwwLDAsMA==",
        2: "source_video/50.avi",
    }

    lines = {1: (330, 270), 2: (340, 280)}

    camera_name = f'camera{args["camera"]}'
    logger = init_logger(camera_name)
    up, down = lines[args["camera"]]

    camera = BusCamera(
        name_camera=camera_name,
        path_to_input_video=rtsp[args["camera"]],
        path_to_output_video="result_video/66.avi",
        logger=logger,
        up=up,
        down=down,
    )

    camera()
