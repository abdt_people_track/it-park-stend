import numpy as np
from os import path, rename
from shutil import copyfile
from sklearn.model_selection import train_test_split

def generate_train_test_dataset(img_name_path:str, path_to_train:str, path_to_test:str, valid_size:float=0.2):
    """Generate train.txt and test.txt from all_img.txt.

    Args:
        img_name_path (str): Path to all images
        path_to_train (str): Path to train
        path_to_test (str): Path to test
        valid_size (float, optional): Test size. Defaults to 0.2.
    """
    if not path.isfile(img_name_path):
        copyfile(path_to_train, img_name_path)

    imgnames = []

    with open(img_name_path, 'r') as file:
        for row in file:
            # row = row.replace('obj_train_data', 'obj')  
            imgnames.append(row)
    train_files, test_files = train_test_split(imgnames, test_size=valid_size, random_state=42)

    # Записываем названия картинок в файлы
    with open(path_to_train, 'w') as file:
        for img in train_files:
            file.write("%s" % img)

    with open(path_to_test, 'w') as file:
        for img in test_files:
            file.write("%s" % img)


def main():
    if path.isdir('whole_frames/obj_train_data'):
        rename('whole_frames/obj_train_data', 'whole_frames/obj')

    img_name_path = 'whole_frames/all_img.txt'
    path_to_train = 'whole_frames/train.txt'
    path_to_test = 'whole_frames/test.txt'

    test_size = 0.2

    generate_train_test_dataset(img_name_path, path_to_train, path_to_test, test_size)


if __name__ == "__main__":
    main()