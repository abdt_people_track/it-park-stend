import os
import numpy as np
import shutil
import cv2


def rename_folder_to_obj(path):
    if os.path.isdir(os.path.join(path, 'obj_train_data')):
        os.rename(os.path.join(path, 'obj_train_data'), os.path.join(path, 'obj'))


def rename_files_in_obj(path, bus_num):
    obj = os.path.join(path, 'obj')
    files = os.listdir(obj)

    for filename in files:
        name, format = filename.split('.')
        if format == 'PNG':
            format = 'jpg'
        name = name + 'bus' + str(bus_num)
        new_name = name + '.' + format
        os.rename(os.path.join(obj, filename), os.path.join(obj, new_name))
    
    temp_list = []
    with open(os.path.join(path, 'train.txt'), 'r') as file:
        for line in file:
            line = line.strip()
            line = line.replace('obj_train_data', 'obj')
            format = 'jpg'
            new_line = line[:-4] + 'bus' + str(bus_num) + '.' + format
            temp_list.append(new_line)

    with open(os.path.join(path, 'train.txt'), 'w') as file:
        for i in temp_list:
            file.write("%s\n" % i)


def skleika(path):
    
    try:
        os.mkdir(os.path.join(path, 'whole_frames/'))
    except:
        print('Папка whole_frames уже существует...')

    try:
        os.mkdir(os.path.join(path, 'whole_frames/obj/'))
    except:
        print('Папка obj в whole_frames уже есть.')

    folders = os.listdir(path)
    file_names = []
    for folder in folders:
        if folder.startswith('task_bus'):
            files_obj = os.listdir(os.path.join(path, folder) + '/obj/')
            for file in files_obj:
                if file.endswith('txt'):
                    with open(os.path.join(os.path.join(path, folder) + '/obj/', file)) as f:
                        if len(f.read()) != 0:
                            image = file[:-3] + 'jpg'
                            txt = file[:-3] + 'txt'
                            if not os.path.isfile(os.path.join(path, 'whole_frames/obj/') + image):
                                file_names.append('data/obj/' + image)
                                shutil.copy(os.path.join(os.path.join(path, folder) + '/obj/', image), os.path.join(path, 'whole_frames/obj/') + image)
                                shutil.copy(os.path.join(os.path.join(path, folder) + '/obj/', txt), os.path.join(path, 'whole_frames/obj/') + txt)
            
    with open(os.path.join(path, 'whole_frames/train.txt'), 'a') as f:
        for i in file_names:
            f.write("%s\n" % i)

def to_grayscale(path='./whole_frames/obj/'):
    files = os.listdir(path)
    for file in files:
        if file.endswith('jpg'):
            img = cv2.imread(os.path.join(path, file))
            img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            img = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)
            os.remove(os.path.join(path, file))
            cv2.imwrite(os.path.join(path, file), img)

if __name__ == '__main__':
    
    """ Запускать этот скрипт с пустой папкой ./whole_frames !!!"""

    # Укажите номер видео task_bus_i, повторяться цифры не могут
    bus = [1, 2, 3, 21, '3_2', '3_10', '3_20', 60, 61, 62, 63, 64, 65, 66, 67, 68, 69] 
    
    # Переименует папки и файлы внутри него для дальнейшей работы
    for num in bus:
        num_bus = str(num)
        path_to_bus = './task_bus_'
        path = path_to_bus + num_bus
        rename_folder_to_obj(path)
        rename_files_in_obj(path, num)
    
    # Создаем папку со всеми фрэймами
    path = './'
    skleika(path)

    # Переводи в ЧБ
    to_grayscale()


