import cv2
import os
import subprocess


def get_current_file_number(path):
    max_number = 0
    files = os.listdir(path)
    for filename in files:
        try:
            if filename.endswith(".avi"):
                max_number = max(int(filename.split(".")[0]), max_number) + 1
        except:
            pass
    return max_number


def open_camera(camera_url, latency=200, width=640, height=480, use_gstreamer=False):
    """Open an RTSP URI (IP CAM)."""
    print("Открываем видео поток ...")
    if use_gstreamer:
        gst_elements = str(subprocess.check_output("gst-inspect-1.0"))
        if "omxh264dec" in gst_elements:
            # Use hardware H.264 decoder on Jetson platforms
            gst_str = (
                "rtspsrc location={} latency={} ! " "rtph264depay ! h264parse ! omxh264dec ! " "nvvidconv ! " "video/x-raw, width=(int){}, height=(int){}, " "format=(string)BGRx ! videoconvert ! " "appsink"
            ).format(camera_url, latency, width, height)
        elif "avdec_h264" in gst_elements:
            # Otherwise try to use the software decoder 'avdec_h264'
            # note: in case resizing images is necessary, try adding
            #       a 'videoscale' into the pipeline
            gst_str = ("rtspsrc location={} latency={} ! " "rtph264depay ! h264parse ! avdec_h264 ! " "videoconvert ! appsink").format(camera_url, latency)
        else:
            raise RuntimeError("H.264 decoder not found!")
        return cv2.VideoCapture(gst_str, cv2.CAP_GSTREAMER)
    else:
        return cv2.VideoCapture(camera_url)


def video_func(camera_url, path_videos, count_of_frames, frame_step):
    cap = open_camera(camera_url, use_gstreamer=False)
    fwidth, fheight = int(cap.get(3)), int(cap.get(4))  # Высота и ширина видеопотока
    fourcc = cv2.VideoWriter_fourcc(*"XVID")

    current_video_num = get_current_file_number(path_videos)
    file_count = current_video_num

    while True:
        print(f"Записываем {path_videos}{file_count}.avi ...")
        out = cv2.VideoWriter(f"{path_videos}{file_count}.avi", fourcc, 20.0, (fwidth, fheight))
        print("Check cap.isOpened(): ", cap.isOpened())
        frame_cnt = 0
        while cap.isOpened() and frame_cnt <= count_of_frames:
            ret, frame = cap.read()
            if not ret:
                print(f"Can't receive frame. Save {frame_cnt} frames. Exiting and out.release ...")
                break
            out.write(frame)
            frame_cnt += 1
            # cv2.imshow('frame', frame)
            if cv2.waitKey(1) == ord("q"):
                break
        out.release()
        file_count += 1
    # end of write videos
    cap.release()
    cv2.destroyAllWindows()


def create_camera_folders(path_videos):
    try:
        os.mkdir(path_videos)
    except Exception:
        print("Папка для видео уже существует!")


if __name__ == "__main__":
    path_videos = "./videos/"
    create_camera_folders(path_videos)

    camera_url = ""

    count_of_frames = 500
    frame_step = 1

    video_func(camera_url, path_videos, count_of_frames, frame_step)
